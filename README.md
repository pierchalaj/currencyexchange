# Exchange currency showcase project

Simple currency exchanger which converts given amount of particular source currency to target currency.
Currency rates are stored in database. Currency codes are implemented as enums. Project is based on RESTful API.

## Tech stack:
- Spring Boot
- Gradle
- Java 8
- h2 Database
- JPA (Hibernate)
- JUnit
- Jackson (for possible future use)
- Project Lombok

## Tools:
- Dev tools to help with live reload

Simple currency exchanger which converts given amount of particular source currency to target currency.
Currency rates are stored in database. Currency codes are implemented as enums. Project is based on RESTful API.

### Rates specified:

1.00 EUR is 1.20 USD

1.00 EUR is 0.90GBP

1.00 EUR is 1.11 CHF

1.00 EUR is 1000.01 JPY


### Endpoint:
`http://localhost:8080/exchangeCurrency`

### Required parameters:
`sourceCurrency`

`targetCurrency`

`amount`

### example:
`http://localhost:8080/exchangeCurrency?sourceCurrency=GBP&targetCurrency=USD&amount=100`

## Response example:
request: `http://localhost:8080/exchangeCurrency?sourceCurrency=EUR&targetCurrency=JPY&amount=100.25`

![https://i.ibb.co/ZBCrFYP/response.png](https://i.ibb.co/ZBCrFYP/response.png)

