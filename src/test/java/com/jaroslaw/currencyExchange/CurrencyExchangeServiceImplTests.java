package com.jaroslaw.currencyExchange;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.jaroslaw.currencyExchange.domain.Currency;
import com.jaroslaw.currencyExchange.domain.ExchangeCurrencyCommand;
import com.jaroslaw.currencyExchange.service.CurrencyExchangeServiceImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import static junit.framework.TestCase.assertTrue;


@RunWith(SpringRunner.class)
@SpringBootTest
public class CurrencyExchangeServiceImplTests {

    @Autowired
    private CurrencyExchangeServiceImpl currencyExchangeService;

    static ExchangeCurrencyCommand euroToEuroMock;
    static ExchangeCurrencyCommand gbpToGbpMock;
    static ExchangeCurrencyCommand euroToGbpMock;
    static ExchangeCurrencyCommand gbpToEuroMock;


    static Map<String, String> euroToNonExisting;
    static Map<String, String> euroToGbpAndNonDigitAmount;
    static Map<String, String> euroToGbpAndNegativeAmount;

    @BeforeClass
    public static void init() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Map> jsonMap = objectMapper.readValue(new File("src/test/resources/testCases.json"), Map.class);
        Map<String, String> euroToEuro = jsonMap.get("euroToEuro");
        Map<String, String> gbpToEuro = jsonMap.get("gbpToEuro");
        Map<String, String> euroToGbp = jsonMap.get("euroToGbp");
        Map<String, String> gbpToGbp = jsonMap.get("gbpToGbp");
        euroToGbpAndNegativeAmount = jsonMap.get("euroToGbpAndNegativeAmount");
        euroToNonExisting = jsonMap.get("euroToNonExisting" );
        euroToGbpAndNonDigitAmount = jsonMap.get("euroToGbpAndNonDigitAmount");


        euroToEuroMock = new ExchangeCurrencyCommand();
        euroToEuroMock.setSourceCurrency(Currency.valueOf(euroToEuro.get("sourceCurrency")));
        euroToEuroMock.setTargetCurrency(Currency.valueOf(euroToEuro.get("targetCurrency")));
        euroToEuroMock.setAmount(String.valueOf(euroToEuro.get("amount")));

        gbpToGbpMock = new ExchangeCurrencyCommand();
        gbpToGbpMock.setSourceCurrency(Currency.valueOf(gbpToGbp.get("sourceCurrency")));
        gbpToGbpMock.setTargetCurrency(Currency.valueOf(gbpToGbp.get("targetCurrency")));
        gbpToGbpMock.setAmount(String.valueOf(gbpToGbp.get("amount")));

        euroToGbpMock = new ExchangeCurrencyCommand();
        euroToGbpMock.setSourceCurrency(Currency.valueOf(euroToGbp.get("sourceCurrency")));
        euroToGbpMock.setTargetCurrency(Currency.valueOf(euroToGbp.get("targetCurrency")));
        euroToGbpMock.setAmount(String.valueOf(euroToGbp.get("amount")));

        gbpToEuroMock = new ExchangeCurrencyCommand();
        gbpToEuroMock.setSourceCurrency(Currency.valueOf(gbpToEuro.get("sourceCurrency")));
        gbpToEuroMock.setTargetCurrency(Currency.valueOf(gbpToEuro.get("targetCurrency")));
        gbpToEuroMock.setAmount(String.valueOf(gbpToEuro.get("amount")));
        System.out.println(gbpToEuroMock.getAmount());

    }

    @Test(expected = IllegalArgumentException.class)
    public void throwsIllegalArgumentExceptionIfCurrencyDoNotExist(){
        ExchangeCurrencyCommand euroToNonExistingMock = new ExchangeCurrencyCommand();
        euroToNonExistingMock.setTargetCurrency(Currency.valueOf(euroToNonExisting.get("targetCurrency")));
    }

    @Test(expected = NumberFormatException.class)
    public void throwsNumberFormatExceptionWhenNegativeAmount(){
        ExchangeCurrencyCommand euroToGbpAndNegativeAmountMock = new ExchangeCurrencyCommand();
        euroToGbpAndNegativeAmountMock.setAmount(String.valueOf(euroToGbpAndNegativeAmount.get("amount")));
    }

    @Test(expected = NumberFormatException.class)
    public void throwsNumberFormatExceptionWhenNonDigitAmount(){
        ExchangeCurrencyCommand euroToGbpAndNonDigitAmountMock = new ExchangeCurrencyCommand();
        euroToGbpAndNonDigitAmountMock.setAmount(String.valueOf(euroToGbpAndNonDigitAmount.get("amount")));
    }

    @Test
    public void amountCalculatedStaysTheSameWhenSourceAndTargetCurrencyTheSame(){
        ExchangeCurrencyCommand exchangeCurrencyCommandAfterCalculation =
                (ExchangeCurrencyCommand) currencyExchangeService.exchangeCurrency(euroToEuroMock).get("success");
        assertTrue(exchangeCurrencyCommandAfterCalculation.getAmount() == 100.25);
        exchangeCurrencyCommandAfterCalculation =
                (ExchangeCurrencyCommand) currencyExchangeService.exchangeCurrency(gbpToGbpMock).get("success");
        assertTrue(exchangeCurrencyCommandAfterCalculation.getAmount() == 100.25);
    }

    @Test
    public void eurToGbpCalculationTest(){
        ExchangeCurrencyCommand exchangeCurrencyCommandAfterCalculation =
                (ExchangeCurrencyCommand) currencyExchangeService.exchangeCurrency(euroToGbpMock).get("success");
        assertTrue(exchangeCurrencyCommandAfterCalculation.getAmount() == 90.23);
    }

    @Test
    public void checkCalculationIfSourceCurrencyIsNotEuro(){
        ExchangeCurrencyCommand exchangeCurrencyCommandAfterCalculation =
                (ExchangeCurrencyCommand) currencyExchangeService.exchangeCurrency(gbpToEuroMock).get("success");
        assertTrue(exchangeCurrencyCommandAfterCalculation.getAmount() == 111.39);
    }

    @Test
    public void checkIfRateIsIncludedInResponseMap(){
        assertTrue(currencyExchangeService.exchangeCurrency(gbpToEuroMock).containsKey("rate"));
    }
}
