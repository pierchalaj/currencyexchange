package com.jaroslaw.currencyExchange.bootstrap;

import com.jaroslaw.currencyExchange.domain.Currency;
import com.jaroslaw.currencyExchange.domain.ExchangeRate;
import com.jaroslaw.currencyExchange.repository.ExchangeRateRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class BootstrapExchangeRates implements InitializingBean {

    @Autowired
    ExchangeRateRepository exchangeRateRepository;

    @Transactional
    public void afterPropertiesSet() throws Exception {
        log.info("Bootstrapping exchange rates...");
        ExchangeRate exchangeRate = new ExchangeRate();
        exchangeRate.setTargetCurrency(exchangeRate.getSourceCurrency());
        exchangeRate.setPrice(1);
        exchangeRateRepository.save(exchangeRate);

        exchangeRate = new ExchangeRate();
        exchangeRate.setTargetCurrency(Currency.GBP);
        exchangeRate.setPrice(0.90);
        exchangeRateRepository.save(exchangeRate);

        exchangeRate = new ExchangeRate();
        exchangeRate.setTargetCurrency(Currency.USD);
        exchangeRate.setPrice(1.20);
        exchangeRateRepository.save(exchangeRate);

        exchangeRate = new ExchangeRate();
        exchangeRate.setTargetCurrency(Currency.CHF);
        exchangeRate.setPrice(1.11);
        exchangeRateRepository.save(exchangeRate);

        exchangeRate = new ExchangeRate();
        exchangeRate.setTargetCurrency(Currency.JPY);
        exchangeRate.setPrice(1000.01);
        exchangeRateRepository.save(exchangeRate);
        log.info("Exchange rates bootstrapping done.");
    }
}
