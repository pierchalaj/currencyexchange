package com.jaroslaw.currencyExchange.repository;

import com.jaroslaw.currencyExchange.domain.Currency;
import com.jaroslaw.currencyExchange.domain.ExchangeRate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExchangeRateRepository extends JpaRepository<ExchangeRate, Long> {

    ExchangeRate findByTargetCurrency(Currency currency);
    ExchangeRate findByPrice(Double price);
}
