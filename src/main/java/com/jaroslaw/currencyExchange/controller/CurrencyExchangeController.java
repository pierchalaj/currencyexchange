package com.jaroslaw.currencyExchange.controller;


import com.jaroslaw.currencyExchange.domain.ExchangeCurrencyCommand;
import com.jaroslaw.currencyExchange.service.CurrencyExchangeServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Collectors;

@RestController
@Slf4j
public class CurrencyExchangeController {

    @Autowired
    CurrencyExchangeServiceImpl currencyExchangeService;

    @GetMapping("/exchangeCurrency")
    public ResponseEntity<Object> getCurrencyExchanged(@ModelAttribute ExchangeCurrencyCommand exchangeCurrencyCommand, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return ResponseEntity.badRequest().body(bindingResult.getAllErrors().stream().map(it -> it.getDefaultMessage()).collect(Collectors.toList()));
        }
        return ResponseEntity.ok(currencyExchangeService.exchangeCurrency(exchangeCurrencyCommand));
    }
}
