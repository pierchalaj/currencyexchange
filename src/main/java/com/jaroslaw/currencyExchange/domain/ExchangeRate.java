package com.jaroslaw.currencyExchange.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

@Entity
@Getter
@Setter
@Slf4j
public class ExchangeRate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;


    @NotNull
    @Enumerated(EnumType.STRING)
    @Setter(AccessLevel.NONE)
    Currency sourceCurrency = Currency.EUR;

    @NotNull
    @Enumerated(EnumType.STRING)
    Currency targetCurrency;

    @NotNull
    double price;

    public void setPrice(double price) {
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols(Locale.GERMAN);
        decimalFormatSymbols.setDecimalSeparator('.');
        DecimalFormat decimalFormat = new DecimalFormat(".##", decimalFormatSymbols);

        this.price = Double.parseDouble(decimalFormat.format(price));
    }

    @Override
    public String toString() {
        return "1" + sourceCurrency + " is " + price + targetCurrency;
    }
}
