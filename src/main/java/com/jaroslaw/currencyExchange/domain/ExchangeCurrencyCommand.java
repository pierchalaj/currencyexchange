package com.jaroslaw.currencyExchange.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

@Component
@Setter
@Getter
@Slf4j
public class ExchangeCurrencyCommand {

    @NotNull
    @Enumerated(EnumType.STRING)
    Currency SourceCurrency;

    @NotNull
    @Enumerated(EnumType.STRING)
    Currency targetCurrency;

    @NotNull
    Double amount;

    public void setAmount(String amount) {
        if(amount.matches("(\\d+[.,]\\d+)|()")){
            DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols(Locale.GERMAN);
            decimalFormatSymbols.setDecimalSeparator('.');
            DecimalFormat decimalFormat = new DecimalFormat(".##", decimalFormatSymbols);
            this.amount = Double.parseDouble(decimalFormat.format(Double.valueOf(amount)));
        }else {
            throw new NumberFormatException("Provide correct price");
        }

    }
}
