package com.jaroslaw.currencyExchange.domain;

public enum Currency {
    GBP(826),
    USD(840),
    CHF(756),
    JPY(392),
    EUR(978);

    private int value;

    Currency(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
