package com.jaroslaw.currencyExchange.service;

import com.jaroslaw.currencyExchange.domain.ExchangeCurrencyCommand;

import java.util.Map;

public interface CurrencyExchangeService {

    Map<String, Object> exchangeCurrency(ExchangeCurrencyCommand exchangeCurrencyCommand);

}
