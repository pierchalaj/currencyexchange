package com.jaroslaw.currencyExchange.service;

import com.jaroslaw.currencyExchange.domain.Currency;
import com.jaroslaw.currencyExchange.domain.ExchangeCurrencyCommand;
import com.jaroslaw.currencyExchange.domain.ExchangeRate;
import com.jaroslaw.currencyExchange.repository.ExchangeRateRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class CurrencyExchangeServiceImpl implements CurrencyExchangeService {

    @Autowired
    ExchangeRateRepository exchangeRateRepository;

    @Override
    public Map<String, Object> exchangeCurrency(ExchangeCurrencyCommand exchangeCurrencyCommand) {
        Map<String, Object> responseMap = new HashMap<>();
        ExchangeRate exchangeRate;
        if(exchangeCurrencyCommand.getSourceCurrency() != exchangeCurrencyCommand.getTargetCurrency()) {
            if (exchangeCurrencyCommand.getSourceCurrency() != Currency.EUR && exchangeCurrencyCommand.getTargetCurrency() != Currency.EUR) {
                ExchangeRate exchangeRateForSource = exchangeRateRepository.findByTargetCurrency(exchangeCurrencyCommand.getSourceCurrency());
                ExchangeRate exchangeRateForTarget = exchangeRateRepository.findByTargetCurrency(exchangeCurrencyCommand.getTargetCurrency());

                double result = (exchangeCurrencyCommand.getAmount() / exchangeRateForSource.getPrice()) * exchangeRateForTarget.getPrice();
                exchangeCurrencyCommand.setAmount(String.valueOf(result));
                responseMap.put("rate", exchangeRateForTarget.toString());

            } else if (exchangeCurrencyCommand.getSourceCurrency() == Currency.EUR) {
                exchangeRate = exchangeRateRepository.findByTargetCurrency(exchangeCurrencyCommand.getTargetCurrency());
                exchangeCurrencyCommand.setAmount(String.valueOf(exchangeCurrencyCommand.getAmount() * exchangeRate.getPrice()));
                responseMap.put("rate", exchangeRate.toString());
            } else {
                exchangeRate = exchangeRateRepository.findByTargetCurrency(exchangeCurrencyCommand.getSourceCurrency());
                exchangeCurrencyCommand.setAmount(String.valueOf(exchangeCurrencyCommand.getAmount() / exchangeRate.getPrice()));
                responseMap.put("rate", exchangeRate.toString());
            }
        }
        responseMap.put("success", exchangeCurrencyCommand);
        return responseMap;
    }
}
